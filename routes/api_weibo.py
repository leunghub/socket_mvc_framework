from utils import log
from routes import json_response
from models.weibo import Weibo
from models.comment import Comment


# weibo
def all(request):
    weibos = Weibo.all_json()
    return json_response(weibos)


def add(request):
    form = request.json()
    w = Weibo.new(form)
    return json_response(w.json())


def delete(request):
    weibo_id = int(request.query.get('id'))
    w = Weibo.delete(weibo_id)
    return json_response(w.json())


def update(request):
    form = request.json()
    weibo_id = int(form.get('id'))
    w = Weibo.update(weibo_id, form)
    return json_response(w.json())


# comment
def comment_all(request):
    comments = Comment.all_json()
    return json_response(comments)


def comment_add(request):
    form = request.json()
    c = Comment.new(form)
    return json_response(c.json())


def route_dict():
    d = {
        # weibo
        '/api/weibo/all': all,
        '/api/weibo/add': add,
        '/api/weibo/delete': delete,
        '/api/weibo/update': update,
        # comment
        '/api/weibo/comment_all': comment_all,
        '/api/weibo/comment_add': comment_add,
    }
    return d