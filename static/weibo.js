// WEIBO API
var apiWeiboAll = function(callback) {
    var path = '/api/weibo/all'
    ajax('GET', path, '', callback)
}

var apiWeiboAdd = function(form, callback) {
    var path = '/api/weibo/add'
    ajax('POST', path, form, callback)
}

var apiWeiboDelete = function(id, callback) {
    var path = '/api/weibo/delete?id=' + id
    ajax('GET', path, '', callback)
}

var apiWeiboUpdate = function(form, callback) {
    var path = '/api/weibo/update'
    ajax('POST', path, form, callback)
}

// WEIBO DOM
var weiboTemplate = function(weibo) {
    var id = weibo.id
    var user_id = weibo.user_id
    var content = weibo.content
    var t = `
        <div class="weibo-cell" data-id="${id}" data-user-id="${user_id}">
            <span class="weibo-content">${content}</span>
            <button class="weibo-delete" data-id="${id}">删除</button>
            <button class="weibo-edit" data-id="${id}">编辑</button>
            <div>
                <input calss='input-comment' data-id="${id}">
                <button class='comment-add' data-id="${id}">添加评论</button>
            </div>
        </div>
        <hr>
    `
    return t
}

var weiboUpdateFormTemplate = function(weibo) {
    var t = `
      <div class="weibo-update-form">
        <input class="weibo-update-input">
        <button class="weibo-update">更新</button>
      </div>
    `
    return t
}

var insertWeibo = function(weibo) {
    var weiboCell = weiboTemplate(weibo)
    var weiboList = e('#weibo-list')
    weiboList.insertAdjacentHTML('beforeend', weiboCell)
}

var loadWeibos = function() {
    apiWeiboAll(function(r) {
        var weibos = JSON.parse(r)
        for(var i = 0; i < weibos.length; i++) {
            var weibo = weibos[i]
            insertWeibo(weibo)
        }
        log('加载微博成功')
    })
}

var bindEventWeiboAdd = function() {
    var b = e('#id-button-add')
    b.addEventListener('click', function(){
        var input = e('#id-input-weibo')
        var content = input.value
        // log('click add', content)
        var form = {
            user_id: null,
            content: content,
        }
        apiWeiboAdd(form, function(r) {
            var weibo = JSON.parse(r)
            insertWeibo(weibo)
        })
    })
}

var bindEventWeiboDelete = function() {
    var weiboList = e('#weibo-list')
    weiboList.addEventListener('click', function(event){
        var self = event.target
        // log(self.classList)

        if (self.classList.contains('weibo-delete')) {
            var weiboId = self.dataset.id
            apiWeiboDelete(weiboId, function(r) {
                // 收到返回的数据, 删除 self 的父节点
                self.parentElement.remove()
            })
          }
      })
}

var bindEventWeiboEdit = function() {
    var weiboList = e('#weibo-list')
    weiboList.addEventListener('click', function(event){
        var self = event.target
        // log(self.classList)
        if (self.classList.contains('weibo-edit')) {
            var t = weiboUpdateFormTemplate()
            self.parentElement.insertAdjacentHTML('beforeend', t)
        }
    })
}

var bindEventWeiboUpdate = function() {
    var weiboList = e('#weibo-list')
    weiboList.addEventListener('click', function(event){
        var self = event.target

        if (self.classList.contains('weibo-update')) {
          var weiboCell = self.closest('.weibo-cell')
          var input = weiboCell.querySelector('.weibo-update-input')
          var id = weiboCell.dataset.id
          var form = {
            id: id,
            content: input.value,
          }
        //   log('form', form)

          apiWeiboUpdate(form, function(r) {
              var updateForm = weiboCell.querySelector('.weibo-update-form')
              updateForm.remove()

              var weibo = JSON.parse(r)
            //   log('weibo json', weibo)
              var content = weiboCell.querySelector('.weibo-content')
              content.innerHTML = weibo.content
          })
      }
    })
}

// COMMENT API
var apiCommentAll = function(callback) {
    var path = '/api/weibo/comment_all'
    ajax('GET', path, '', callback)
}

// COMMENT DOM
var commentTemplate = function(comment) {
    // var username = comment
    var id = comment.weibo_id
    var content = comment.content
    var t = `
        <div class="comment-cell" data-weibo-id="${id}">
            <span>${content}</span>
        </div>
    `
    return t
}

var insertComment = function(comment) {
    var id = comment.weibo_id
    var commentCell = commentTemplate(comment)
    var weiboCell = e(`.weibo-cell[data-id="${id}"]`)
    // log(weiboCell)
    weiboCell.insertAdjacentHTML('beforeEnd', commentCell)
}

var loadComments = function() {
    apiCommentAll(function(r) {
        var comments = JSON.parse(r)
        weiboCells = document.querySelectorAll('.weibo-cell')
        for(let i = 0; i < weiboCells.length; i++) {
            id = weiboCells[i].dataset.id
            for (let i = 0; i < comments.length; i++) {
                comment = comments[i]
                if (id == comment.weibo_id) {
                    // log('匹配数据成功, 执行插入', comment.content)
                    insertComment(comment)
                }
            }
        }
        log('加载评论完成')
    })
}

var bindEventCommentAdd = function() {
    // var weiboList = e('#weibo-list')
    var commentCell = e('.comment-cell')
    commentCell.addEventListener('click', function(event){
        var self = event.target
        var id = self.dataset.id
        // log(`${id}`)
        if (self.classList.contains('comment-add')) {
            // log(`.input-comment[data-id="${id}"]`)
            var input = e(`.input-comment[data-id="${id}"]`)
            // var input = e(`.input-comment[data-id="3"]`)
            log(input)
            // log(input.value)
        }
    })
}

var bindEvents = function() {
    bindEventWeiboAdd()
    bindEventWeiboDelete()
    bindEventWeiboEdit()
    bindEventWeiboUpdate()
    // comment
    bindEventCommentAdd()
}

var __main = function() {
    loadWeibos()
    loadComments()
    bindEvents()
}

__main()
